# myFlix

This project is an REST API based on the MERN/MEAN stack. This API provide users with access to information about different movies, directors, and genres. Users will be able to sign up, update their personal information, and create a list of their favorite movies.

[The documentation here](https://primedome.herokuapp.com/documentation.html)

## Features

- Return a list of ALL movies to the user
- Return data (description, genre, director, image URL) about a single movie by title to the user
- Return data about a genre (description) by name/title (e.g., “Thriller”)
- Return data about a director (bio, birth year, death year) by name
- Allow new users to register
- Allow users to update their user info (username, password, email, date of birth)
- Allow users to add a movie to their list of favorites
- Allow users to remove a movie from their list of favorites
- Allow existing users to deregister

## Technologies

- Build on [Node.js](https://nodejs.org/en/) and [Express](http://expressjs.com)
- Database [MongoDB](https://www.mongodb.com) with [Mongoose](https://mongoosejs.com)
- Tested on [Postman](https://www.postman.com)
- Deployed on [Heroku](https://www.heroku.com/home)

## Frontend

These two application use this REST API:

[myFlix-react-client](https://github.com/ximnoise/myFlix-react-client)

[myFlix-angular-client](https://github.com/ximnoise/myFlix-angular-client)

